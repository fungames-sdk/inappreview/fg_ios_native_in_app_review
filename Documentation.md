IOS Native In App Review
------------------------------------------------------------------

Integration Steps
------------------------------------------------------------------
1. Install or upload FG InAppReview plugin from the FunGames Integration Manager.

2. Follow the instructions in the "Configure Project Settings" section to set up the necessary components for iOS Native In App Review.

3. Click on the "Prefabs and Settings" button in the FunGames Integration Manager to fill up your scene with the required components and create the Settings asset.

4. To finish your integration, follow the Account and Settings section.

Configure Project Settings
------------------------------------------------------------------
1.Open your project in Unity.

2.Go to Edit > Project Settings > Player.

3. In the iOS tab, make sure you have set up your bundle identifier, version, and other necessary settings.

4. Ensure that your project is configured to support iOS 10.3 or later, as the In App Review API requires this version or higher.

Account and Settings
------------------------------------------------------------------
1. Open the FGiOSInAppReviewSettings asset in the Resources folder (Assets > Resources > FunGames > FGiOSInAppReviewSettings).

2. Configure the settings as needed for your app.

To test the integration, please first run your game by building and running it on your iOS device as the review prompt only works on mobile. Once the game is launched, trigger the in app review process to ensure everything is working correctly.
